var http = require('http');
 
var accesskey = 'your_device_accesskey';
var topic_id = 'your_device_topic_id';
var dataString;
 
function getSensor(accesskey) {      
 
    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': 0,
        'Authorization': "Bearer " + accesskey
    };
 
    var options = {
        host: 'api.alooh.io',
        port: 50001,
        path: '/api/v1/topics/'+topic_id+'/getsensor',
        method: 'POST',
        headers: headers
    };    
    
    callback = function(response) {
        var str = '';
        response.on('data', function (chunk) {
            str += chunk;
        });
        response.on('end', function () {
            console.log(str);
        });
    };
 
    var req = http.request(options, callback);    
    req.end(dataString);
 
    console.log(options);
};
 
getSensor(accesskey);