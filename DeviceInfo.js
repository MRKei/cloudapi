var http = require('http');
var auid= 'your_device_auid';
var accesskey='your_device_accesskey';
 
function device_info() {
    var data = {
        'data': {
            'auid': auid,
            'type': "3"// 키 전체(1:디바이스, 2:app, 3:전체)            
        }
    };
 
    var dataString = JSON.stringify(data);
 
    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': dataString.length,
        'Authorization': "Bearer " + accesskey
    };
 
    var options = {
        host: 'api.alooh.io',
        port: 50001,
        path: '/api/v1/devices/auid',
        method: 'POST',
        headers: headers
    };
    callback = function(response) {
        var str = '';
        response.on('data', function (chunk) {
            str += chunk;
        });
        response.on('end', function () {
            console.log(str);//수신한 정보 출력
        });
    };
    var req = http.request(options, callback);
    req.write(dataString);
    req.end();
};
 
device_info(); //실행