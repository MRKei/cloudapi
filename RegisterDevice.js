var http = require('http');
var auid= 'your_device_auid';
var key;
var name = 'your_device_name';
 
session_callback = function (response) {
    var str='';
    response.on('data', function (chunk) {
        str += chunk;
    });
 
    response.on('end', function () {
        console.log(str);       
 
        if (str.indexOf("error") == -1) {
            var dStr = JSON.parse(str);
 
            if (dStr.data.key) {
                key = dStr.data.key;
                Register();
            }
        }
    });
}
 
function Register() {
 
    var data = {
        'data': {
            'auid': auid,
            'name': name
        }
    };
 
    var dataString = JSON.stringify(data);
 
    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': dataString.length,
        'Authorization': "Bearer " + key
    };
    
    var options = {
        host: 'api.alooh.io',
        port: 50001,
        path: '/api/v1/devices/register',
        method: 'POST',
        headers: headers
    };
 
    console.log(dataString);
    
    callback = function(response) {
        var str = '';
        response.on('data', function (chunk) {
            str += chunk;
        });
        response.on('end', function () {
            console.log(str);
        });
    };
 
    var req = http.request(options, callback);    
    req.end(dataString);
    
    console.log(options);
};
 
function session() {
 
    var data = {
        'data': {
            'email': 'user_email',
            'password': 'user_password'
        }
    };
 
    var dataString = JSON.stringify(data);
 
    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': dataString.length,
    };
 
    var options = {
        host: 'api.alooh.io',
        port: 50001,
        path: '/api/v1/session',
        method: 'POST',
        headers: headers
    };
 
    console.log(dataString);
 
    var req = http.request(options, session_callback);    
    req.end(dataString);
};
 
session();