var http = require('http');
var name = 'your_device_name';
var email = 'your_email';
var password= 'your_pw';
 
getkey = function (response) {
    var str='';
    response.on('data', function (chunk) {
        str += chunk;
    });
 
    response.on('end', function () {           
 
        if (str.indexOf("error") == -1) {
            var dStr = JSON.parse(str);
 
            if (dStr.data.key) {
                key = dStr.data.key;
                Check(key);
            }
        }
    });
}
 
function Check(key) {//디바이스 정보 검색
 
    var data = {
        'data': {
            'name': name
        }
    }; 
 
    var dataString = JSON.stringify(data);
 
    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': dataString.length,
        'Authorization': "Bearer " + key
    };
 
    var options = {
        host: 'api.alooh.io',
        port: 50001,
        path: '/api/v1/devices/check/name',
        method: 'POST',
        headers: headers
    };
    
    callback = function(response) {
        var str = '';
        response.on('data', function (chunk) {
            str += chunk;
        });
        response.on('end', function () {
            console.log(str);
        });
    };
    var req = http.request(options, callback);
    req.end(dataString); 
};
 
function session() {//접근 키 발급
 
    var data = {
        'data': {
            'email': email,
            'password': password
        }
    };
 
    var dataString = JSON.stringify(data);
 
    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': dataString.length,
    };
    
    var options = {
        host: 'api.alooh.io',
        port: 50001,
        path: '/api/v1/session',
        method: 'POST',
        headers: headers
    };    
 
    var req = http.request(options, getkey);    
    req.end(dataString);
};
 
session();//실행