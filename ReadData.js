var http = require('http');
 
var topic_id='your_device_topic_id';
var accesskey = 'your_device_accesskey';
 
function readData(accesskey) {
 
    var data = {
        'data': {
            'start': '2015-04-15 17:05:00',
            'end': '2015-04-16 00:37:00',
            'count': 1,
            'sensor': 'a'
        }
    }; 
 
    var dataString = JSON.stringify(data);
 
    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': dataString.length,
        'Authorization': "Bearer " + accesskey
    };
 
    var options = {
        host: 'api.alooh.io',
        port: 50001,
        path: '/api/v1/topics/'+topic_id+'/read',
        method: 'POST',
        headers: headers
    };
 
    console.log(dataString);
    
    callback = function(response) {
        var str = '';
        response.on('data', function (chunk) {
            str += chunk;
        });
        response.on('end', function () {
            console.log(str);
        });
    };
 
    var req = http.request(options, callback);
    req.end(dataString);
 
    console.log(options);
};
 
readData(accesskey);