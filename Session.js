var http = require('http');

var email = 'user_email';
var password= 'user_password';
var key; 

getkey = function (response) {
    var str='';
    response.on('data', function (chunk) {
        str += chunk;
    });
 
    response.on('end', function () {           
 
        if (str.indexOf("error") == -1) {
            var dStr = JSON.parse(str);
            
            if (dStr.data.key) {
                key = dStr.data.key;// key
            }
        }
    });
};

function session() {//접근 키 발급
 
    var data = {
        'data': {
            'email': email,
            'password': password
        }
    };
 
    var dataString = JSON.stringify(data);
 
    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': dataString.length,
    };
    
    var options = {
        host: 'api.alooh.io',
        port: 50001,
        path: '/api/v1/session',
        method: 'POST',
        headers: headers
    };    
 
    var req = http.request(options,getkey);  
    req.end(dataString);
    
};
session();