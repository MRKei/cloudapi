var http = require('http');

var auid= 'your_device_auid';
var name = 'your_device_name';
var ip = 'your_device_ip';
var accesskey = 'your_device_accesskey';
var check_update='check_parameter';

function Update(accesskey) {
 
    var data = {
        'data': {
            'name': name,               // 디바이스 이름
            'status': "OFF",            // “ON” or "OFF"
            'ip': ip,                   // 디바이스 IP 주소 변경을 위한 파라미터.
            'checkUpdate':check_update   // APP에서 업데이트 성공여부를 확인하기 위한 파라미터
        }
    }; 
 
    var dataString = JSON.stringify(data);
 
    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': dataString.length,
        'Authorization': "Bearer " + accesskey
    };
 
    var options = {
        host: 'api.alooh.io',
        port: 50001,
        path: '/api/v1/devices/update',
        method: 'POST',
        headers: headers
    };
    
    callback = function(response) {
        var str = '';
        response.on('data', function (chunk) {
            str += chunk;
        });
        response.on('end', function () {
            console.log(str);
        });
    };
 
    var req = http.request(options, callback);
    req.end(dataString);
};
Update(accesskey);